    </div>
    <!-- MAIN FOOTER -->
    <footer id="nasa-footer" class="footer-wrapper">
        <?php do_action('nasa_footer_layout_style'); ?>
    </footer>
    <!-- END MAIN FOOTER -->
</div>
<?php wp_footer(); ?>

<script>
jQuery('body').on('touchstart', '.product-img a', function(e) {
    'use strict';
    var link = jQuery(this);
    if (link.hasClass('hover')) {
        link.attr("href", link.data("href"));
    } else {
        link.addClass('hover');
        jQuery('.product-img a').not(this).removeClass('hover');
        link.data("href", link.attr("href")).removeAttr("href").css("cursor","pointer");
    }
});

jQuery(document).ready(function($) {
    
    $("#size-chart").appendTo($(".woocommerce-product-details__short-description"));
    $("#chart-button").remove();
});



jQuery(document).ready(function () {
    jQuery(".textarea-hadiah").hide()
    jQuery("#myfield1_checkbox").click(function () {
        if (jQuery(this).is(":checked")) {
            jQuery(".textarea-hadiah").show();
        } else {
            jQuery(".textarea-hadiah").hide();
            jQuery("#myfield2").val("");
        }
    });

});

</script>
</body>
</html>