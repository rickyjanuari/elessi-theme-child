jQuery(window).trigger("resize").trigger("scroll");
var doc = document.documentElement;
doc.setAttribute("data-useragent", navigator.userAgent);
var wow_enable = !1,
    fullwidth = 1200,
    iOS = check_iOS(),
    _event = iOS ? "click, mousemove" : "click",
    globalTimeout = null,
    load_flag = !1,
    page_load = 1,
    shop_load = !1,
    archive_page = 1,
    infinitiAjax = !1,
    _single_variations = [],
    _lightbox_variations = [];
jQuery(document).ready(function(a) {
    "use strict";
    a(window).stellar(), 1 === a('input[name="nasa-enable-wow"]').length && "1" === a('input[name="nasa-enable-wow"]').val() && (wow_enable = !0, a("body").addClass("nasa-enable-wow"), new WOW({
        mobile: !1
    }).init()), a("body #nasa-before-load").fadeOut(1e3), a("body").addClass("nasa-body-loaded"), "undefined" == typeof nasa_ajax_setup && a.ajaxSetup({
        data: {
            context: "frontend"
        }
    }), a(document).ajaxComplete(function() {
        a(".btn-wishlist").removeClass("nasa-disabled"), setTimeout(function() {
            a(".nasa_yith_wishlist_premium-wrap").length && a(".nasa-wishlist-count.wishlist-number").length && a(".nasa_yith_wishlist_premium-wrap").each(function() {
                var t = a(this);
                if (!a(t).parents(".wishlist_sidebar").length) {
                    var e = a(t).find(".wishlist_table tbody tr .wishlist-empty").length ? "0" : a(t).find(".wishlist_table tbody tr").length;
                    a(".nasa-wishlist-count.wishlist-number .nasa-sl").html(e), "0" === e && a(".nasa-wishlist-count.wishlist-number").addClass("nasa-product-empty")
                }
            })
        }, 300)
    }).ajaxError(function() {
        a(".btn-wishlist").removeClass("nasa-disabled")
    });
    var t = {
        action: "nasa_get_static_content",
        context: "frontend"
    };
    if (1 === a("#nasa-view-compare-product").length && (t.compare = !0), a.ajax({
            url: ajaxurl,
            type: "post",
            dataType: "json",
            data: t,
            beforeSend: function() {},
            success: function(t) {
                "undefined" != typeof t && a.each(t, function(t, e) {
                    a(t).length > 0 && (a(t).replaceWith(e), "#nasa-wishlist-sidebar-content" === t && initWishlistIcons(a), "#nasa-compare-sidebar-content" === t && initCompareIcons(a))
                })
            }
        }), a("#wpadminbar").length > 0) {
        a("head").append('<style type="text/css" media="screen">#wpadminbar {position: fixed !important;}</style>');
        var e = a("#wpadminbar").height();
        a("#cart-sidebar, #nasa-wishlist-sidebar, #nasa-viewed-sidebar, #nasa-quickview-sidebar, .col-sidebar").css({
            top: e
        }), a(window).resize(function() {
            e = a("#wpadminbar").height(), a("#cart-sidebar, #nasa-wishlist-sidebar, #nasa-viewed-sidebar, #nasa-quickview-sidebar, .col-sidebar").css({
                top: e
            })
        })
    }
    if (a(".vertical-menu-wrapper").length > 0) {
        a(".vertical-menu-wrapper").attr("data-over", "0");
        var n = 200;
        a(".vertical-menu-container").each(function() {
            var t = a(this),
                e = a(t).height();
            a(t).find(".nasa-megamenu >.nav-dropdown").each(function() {
                a(this).css({
                    width: 0
                }), a(this).find(">.sub-menu").css({
                    "min-height": e
                })
            })
        }), a("body").on("mousemove", ".vertical-menu-container .nasa-megamenu", function() {
            var t = a(this).parents(".vertical-menu-wrapper");
            a(t).find(".nasa-megamenu").removeClass("nasa-curent-hover"), a(t).addClass("nasa-curent-hover");
            var e, s, i, r = a(t).parents(".row").length ? a(t).parents(".row") : a(t).parents(".nasa-row"),
                o = a(r).length ? a(r).width() : 900;
            a(t).find(".nasa-megamenu").each(function() {
                var r = a(this),
                    l = a(r).outerWidth();
                if (e = s = o - l, a(r).hasClass("cols-5") || a(r).hasClass("fullwidth") ? e -= 20 : a(r).hasClass("cols-2") ? (e = e / 5 * 2 + 50, i = 2 * n, e = i > e && s > i ? i : e) : a(r).hasClass("cols-3") ? (e = e / 5 * 3 + 50, i = 3 * n, e = i > e && s > i ? i : e) : a(r).hasClass("cols-4") && (e = e / 5 * 4 + 50, i = 4 * n, e = i > e && s > i ? i : e), a(r).hasClass("nasa-curent-hover")) {
                    var d = a(t).attr("data-over");
                    "0" === d ? (a(t).attr("data-over", "1"), a(r).find(">.nav-dropdown").css({
                        width: 0
                    }).animate({
                        width: e
                    }, 50)) : a(r).find(">.nav-dropdown").css({
                        width: e
                    })
                } else a(r).find(">.nav-dropdown").css({
                    width: e
                })
            })
        }), a("body").on("mouseover", ".vertical-menu-wrapper .menu-item-has-children.default-menu", function() {
            var t = a(this).parents(".vertical-menu-wrapper"),
                e = a(t).attr("data-over");
            "0" === e ? (a(t).attr("data-over", "1"), a(this).find("> .nav-dropdown > .sub-menu").css({
                width: 0
            }).animate({
                width: n
            }, 150)) : a(this).find("> .nav-dropdown > .sub-menu").css({
                width: n
            });
            var s, i, r, o = a(t).parents(".row").length ? a(t).parents(".row") : a(t).parents(".nasa-row"),
                l = a(o).length ? a(o).width() : 900;
            a(t).find(".nasa-megamenu").each(function() {
                var t = a(this),
                    e = a(t).outerWidth();
                s = i = l - e, a(t).hasClass("cols-5") || a(t).hasClass("fullwidth") ? s -= 20 : a(t).hasClass("cols-2") ? (s = s / 5 * 2 + 50, r = 2 * n, s = r > s && i > r ? r : s) : a(t).hasClass("cols-3") ? (s = s / 5 * 3 + 50, r = 3 * n, s = r > s && i > r ? r : s) : a(t).hasClass("cols-4") && (s = s / 5 * 4 + 50, r = 4 * n, s = r > s && i > r ? r : s), a(t).find(">.nav-dropdown").css({
                    width: s
                })
            })
        }), a("body").on("mouseleave", ".vertical-menu-wrapper", function() {
            a(this).attr("data-over", "0"), a(this).find(".nasa-megamenu > .nav-dropdown").css({
                width: 0
            }), a(this).find(".menu-item-has-children.default-menu > .nav-dropdown > .sub-menu").css({
                width: 0
            })
        })
    }
    a("body").on("click", ".nasa-mobile-menu_toggle", function() {
        initMainMenuVertical(a), a("#mobile-navigation").length && ("1" !== a("#mobile-navigation").attr("data-show") ? (a("#nasa-menu-sidebar-content").css({
            "z-index": 9999
        }), a(".black-window").show().addClass("desk-window").addClass("nasa-transparent"), a("#nasa-menu-sidebar-content").show().animate({
            left: 0
        }, 800), a("#mobile-navigation").attr("data-show", "1")) : a(".black-window").click())
    }), a("body").on("click", ".nasa-menu-accordion .li_accordion > a.accordion", function(t) {
        t.preventDefault();
        var e = a(this).parent(),
            n = a(e).parent();
        if (a(e).hasClass("active")) a(e).find(">.nav-dropdown-mobile").slideUp(300).parent().removeClass("active"), a(this).find("span").removeClass("fa-minus-square-o").addClass("fa-plus-square-o");
        else {
            var s = a(n).children("li.active");
            a(s).removeClass("active").children(".nav-dropdown-mobile").css({
                height: "auto"
            }).slideUp(300), a(e).children(".nav-dropdown-mobile").slideDown(300).parent().addClass("active"), a(s).find("> a.accordion > span").removeClass("fa-minus-square-o").addClass("fa-plus-square-o"), a(this).find("span").removeClass("fa-plus-square-o").addClass("fa-minus-square-o")
        }
        return !1
    }), a(".nasa-accordion .li_accordion > a.accordion").length > 0 && a("body").on("click", ".nasa-accordion .li_accordion > a.accordion", function() {
        var t = a(this).attr("data-class_show"),
            e = a(this).attr("data-class_hide"),
            n = a(this).parent(),
            s = a(n).parent();
        if (a(n).hasClass("active")) a(n).removeClass("active").children(".children").slideUp(300), a(this).find("span").removeClass(e).addClass(t);
        else {
            a(s).removeClass("current-cat-parent").removeClass("current-cat");
            var i = a(s).children("li.active");
            a(i).removeClass("active").children(".children").slideUp(300), a(n).addClass("active").children(".children").slideDown(300), a(i).find(">a.accordion>span").removeClass(e).addClass(t), a(this).find("span").removeClass(t).addClass(e)
        }
        return !1
    }), a("body").on("click", ".quick-view", function(t) {
        var e = a(this),
            n = a(e).attr("data-product_type");
        if (a(e).parents(".product-item").length && "woosb" !== n) {
            var s = a(this).parents(".product-item");
            a(s).hasClass("nasa-quickview-special") ? a(s).append('<div class="nasa-loader" style="top:50%"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>') : (a(s).find(".product-inner").css({
                opacity: .3
            }), a(s).find(".product-inner").after('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>'))
        }
        if (a(e).parents(".item-product-widget").length && "woosb" !== n && a(e).parents(".item-product-widget").append('<div class="nasa-light-fog"></div><div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>'), "woosb" === n && "undefined" != typeof a(e).attr("data-href")) window.location.href = a(e).attr("data-href");
        else {
            var i = a(e).parents(".product-item"),
                r = a(i).find(".product-inner"),
                o = a(r).find(".product-img"),
                l = a(e).attr("data-prod"),
                d = "1" === a(e).attr("data-from_wishlist") ? "1" : "0";
            a(i).length <= 0 && (i = a(e).parents(".item-product-widget")), a(i).length <= 0 && (i = a(e).parents(".wishlist-item-warper"));
            var c = {
                    action: "nasa_quickview",
                    product: l,
                    nasa_wishlist: d
                },
                p = 1 === a("#nasa-quickview-sidebar").length ? !0 : !1;
            c.quickview = 1 === a("#nasa-quickview-sidebar").length ? "sidebar" : "popup", a.ajax({
                url: ajaxurl,
                type: "post",
                dataType: "json",
                data: c,
                beforeSend: function() {
                    p && (a("#nasa-quickview-sidebar #nasa-quickview-sidebar-content").html('<div class="nasa-absolute nasa-center" style="width: 100%; height: 80%"><div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div></div>'), a(".black-window").fadeIn(200).addClass("desk-window"), a("#nasa-viewed-sidebar").show().animate({
                        right: "-100%"
                    }, 1e3).hide(1e3), a("#nasa-quickview-sidebar").show().addClass("nasa-active").animate({
                        right: 0
                    }, 1e3)), a(".nasa-static-wrap-cart-wishlist").length && a(".nasa-static-wrap-cart-wishlist").hasClass("nasa-active") && a(".nasa-static-wrap-cart-wishlist").removeClass("nasa-active")
                },
                success: function(t) {
                    p ? (a("#nasa-quickview-sidebar #nasa-quickview-sidebar-content").html('<div class="product-lightbox hidden-tag">' + t.content + "</div>"), setTimeout(function() {
                        a("#nasa-quickview-sidebar #nasa-quickview-sidebar-content .product-lightbox").fadeIn(1e3)
                    }, 600)) : (a.magnificPopup.open({
                        mainClass: "my-mfp-zoom-in",
                        items: {
                            src: '<div class="product-lightbox">' + t.content + "</div>",
                            type: "inline"
                        },
                        tClose: a('input[name="nasa-close-string"]').val(),
                        callbacks: {
                            afterClose: function() {
                                var t = a(e).parents(".product-interactions");
                                a(t).addClass("hidden-tag"), setTimeout(function() {
                                    a(t).removeClass("hidden-tag")
                                }, 100)
                            }
                        }
                    }), a(".black-window").click()), _lightbox_variations[0] = {
                        quickview_gallery: a(".product-lightbox").find(".nasa-product-gallery-lightbox").html()
                    }, a(e).hasClass("nasa-view-from-wishlist") && (a(".wishlist-item").animate({
                        opacity: 1
                    }, 500), p || a(".wishlist-close a").click()), a(i).find(".nasa-loader, .please-wait, .color-overlay, .nasa-dark-fog, .nasa-light-fog").remove(), a(o).length > 0 && a(r).length > 0 && (a(o).removeAttr("style"), a(r).animate({
                        opacity: 1
                    }, 500));
                    var n = a(".product-lightbox").find(".variations_form");
                    1 === a(n).find(".single_variation_wrap").length && (a(n).find(".single_variation_wrap").hide(), a(n).wc_variation_form_lightbox(t.mess_unavailable), a(n).find("select").change(), a(n).find('.variations select option[selected="selected"]').length && a(n).find(".variations .reset_variations").css({
                        visibility: "visible"
                    }).show(), 1 === a('input[name="nasa_attr_ux"]').length && "1" === a('input[name="nasa_attr_ux"]').val() && a(n).nasa_attr_ux_variation_form()), setTimeout(function() {
                        loadLightboxCarousel(a), loadTipTop(a)
                    }, 600), setTimeout(function() {
                        a(window).resize()
                    }, 800), p || setTimeout(function() {
                        var t = a(".product-lightbox .product-lightbox-inner").outerHeight(),
                            e = a(".product-lightbox .product-img").outerHeight();
                        e > 0 && t > e && a(".product-lightbox .product-img").css({
                            position: "relative",
                            top: "0"
                        }).animate({
                            top: (t - e) / 2
                        }, 300)
                    }, 2600), a(".nasa-quickview-product-deal-countdown").length && loadCountDown(a)
                }
            })
        }
        t.preventDefault()
    }), a(".gallery a[href$='.jpg'], .gallery a[href$='.jpeg'], .featured-item a[href$='.jpeg'], .featured-item a[href$='.gif'], .featured-item a[href$='.jpg'], .page-featured-item .slider > a, .page-featured-item .page-inner a > img, .gallery a[href$='.png'], .gallery a[href$='.jpeg'], .gallery a[href$='.gif']").parent().magnificPopup({
        delegate: "a",
        type: "image",
        tLoading: '<div class="please-wait dark"><span></span><span></span><span></span></div>',
        tClose: a('input[name="nasa-close-string"]').val(),
        mainClass: "my-mfp-zoom-in",
        gallery: {
            enabled: !0,
            navigateByImgClick: !0,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });
    var s, i = setTimeout(function() {
            loadHeightFullWidthToSide(a), loadResponsiveMainMenu(a)
        }, 100),
        r = setTimeout(function() {
            row_equal_height_columns(a)
        }, 100),
        o = a(".mobile-menu").length > 0 ? a(".mobile-menu").height() + 50 : 0,
        l = a(".header-wrapper").height() + 50;
    a(window).scroll(function() {
        var t = a(this).scrollTop();
        if (a('input[name="nasa_fixed_single_add_to_cart"]').length && "1" === a('input[name="nasa_fixed_single_add_to_cart"]').val() && a(".nasa-product-details-page .single_add_to_cart_button").length) {
            var e = a(".nasa-product-details-page .product-details") || a(".nasa-product-details-page .single_add_to_cart_button"),
                n = a(e).offset();
            t >= n.top ? a("body").hasClass("has-nasa-cart-fixed") || a("body").addClass("has-nasa-cart-fixed") : a("body").removeClass("has-nasa-cart-fixed")
        }
        if (a("#nasa-wrap-archive-loadmore.nasa-infinite-shop").length > 0 && 1 === a("#nasa-wrap-archive-loadmore.nasa-infinite-shop").find(".nasa-archive-loadmore").length) {
            var i = a("#nasa-wrap-archive-loadmore").offset();
            infinitiAjax || t + a(window).height() >= i.top && (infinitiAjax = !0, a("#nasa-wrap-archive-loadmore.nasa-infinite-shop").find(".nasa-archive-loadmore").click())
        }
        var r = a(".nasa-check-reponsive.nasa-switch-check").length && 1 === a(".nasa-check-reponsive.nasa-switch-check").width() ? !0 : !1;
        if (a("body").find(".nasa-header-sticky").length > 0) {
            var d = a(".sticky-wrapper"),
                c = 0;
            a("#wpadminbar").length > 0 && (c = a("#wpadminbar").height());
            var p = d.outerHeight();
            t > l ? d.hasClass("fixed-already") || (d.stop().addClass("fixed-already"), a(".nasa-header-sticky").css({
                "margin-bottom": p
            }), d.hasClass("fixed-trasition") || setTimeout(function() {
                d.css({
                    top: c
                }), d.addClass("fixed-trasition")
            }, 10)) : (d.hasClass("fixed-already") && (d.stop().removeClass("fixed-already"), d.removeAttr("style"), a(".nasa-header-sticky").removeAttr("style")), d.hasClass("fixed-trasition") && d.stop().removeClass("fixed-trasition"), p = d.outerHeight())
        }
        if (a(".nasa-nav-extra-warp").length > 0 && (t > l ? a(".nasa-nav-extra-warp").hasClass("nasa-show") || a(".nasa-nav-extra-warp").addClass("nasa-show") : a(".nasa-nav-extra-warp").hasClass("nasa-show") && a(".nasa-nav-extra-warp").removeClass("nasa-show")), l >= t && "1" === a("#mobile-navigation").attr("data-show") && !r && a(".black-window").click(), a("#nasa-back-to-top").length > 0) {
            var h = a(window).height() / 2;
            if (t > h) {
                var u = a("#nasa-back-to-top").attr("data-wow");
                a("#nasa-back-to-top").show().css({
                    visibility: "visible",
                    "animation-name": u
                }).removeClass("animated").addClass("animated")
            } else a("#nasa-back-to-top").hide()
        }
        if (a(".mobile-menu").length > 0 && a(".nasa-has-fixed-header").length) {
            var m = a(".mobile-menu").parent();
            if (t > o + 50) {
                if (!a(".mobile-menu").hasClass("nasa-mobile-fixed")) {
                    var f = a("#wpadminbar").length > 0 ? a("#wpadminbar").height() : 0;
                    a(".mobile-menu").addClass("nasa-mobile-fixed"), a(".mobile-menu").css({
                        top: 0 + f
                    }), a(m).hasClass("nasa-padding-only-mobile") || a(m).addClass("nasa-padding-only-mobile"), a(m).css({
                        "padding-top": a(".mobile-menu").height()
                    })
                }
            } else a(".mobile-menu").hasClass("nasa-mobile-fixed") && (a(".mobile-menu").removeClass("nasa-mobile-fixed").removeAttr("style"), a(m).removeAttr("style"))
        }
        clearTimeout(s), s = setTimeout(function() {
            row_equal_height_columns(a, !0)
        }, 100)
    }), a("body").on("click", "#nasa-back-to-top", function() {
        a("html, body").animate({
            scrollTop: 0
        }, 800)
    }), a(window).resize(function() {
        var t = a(".nasa-check-reponsive.nasa-switch-check").length && 1 === a(".nasa-check-reponsive.nasa-switch-check").width() ? !0 : !1,
            e = a(".black-window").hasClass("desk-window");
        if (!t && !e) {
            if (a(".col-sidebar").length > 0)
                if (a(".nasa-toggle-topbar").length > 0) {
                    var n = a(".nasa-toggle-topbar").attr("data-filter");
                    "1" === n ? a(".col-sidebar").removeAttr("style") : a(".col-sidebar").hide()
                } else a(".col-sidebar").removeAttr("style");
            a(".warpper-mobile-search").length > 0 && !a(".warpper-mobile-search").hasClass("show-in-desk") && a(".warpper-mobile-search").hide(), a(".black-window").length > 0 && a(".black-window").hide()
        }
        if (initTopCategoriesFilter(a), a(".wide-nav .nasa-vertical-header").length > 0) {
            var s = a(".wide-nav .nasa-vertical-header").width();
            s = 280 > s ? 280 : s, a(".wide-nav .vertical-menu-container").css({
                width: s
            })
        }
        if (a(".nasa-slide-style").length > 0) {
            var o;
            a(".nasa-slide-style").each(function() {
                var t = a(this);
                o = 0, a(t).find(".nasa-tabs .nasa-tab").each(function() {
                    o += a(this).outerWidth()
                }), o > a(t).width() ? a(t).find(".nasa-tabs .nasa-tab").hasClass("nasa-block") || a(t).find(".nasa-tabs .nasa-tab").addClass("nasa-block") : a(t).find(".nasa-tabs .nasa-tab").removeClass("nasa-block")
            })
        }
        var l = a("#wpadminbar").length > 0 ? a("#wpadminbar").height() : 0;
        if (l > 0 && 1 === a("#mobile-navigation").length && (a("#nasa-menu-sidebar-content").css({
                top: l
            }), "1" === a("#mobile-navigation").attr("data-show") && !t)) {
            var c = a(window).scrollTop(),
                p = a(".header-wrapper").height() + 50;
            p >= c && a(".black-window").click()
        }
        if ("1" !== a("#mobile-navigation").attr("data-show") && a("#nasa-menu-sidebar-content").length > 0) {
            var h = a("#nasa-menu-sidebar-content").outerWidth().toString();
            a("#nasa-menu-sidebar-content").css({
                left: "-" + h + "px"
            })
        }
        loadScrollSingleProduct(a), clearTimeout(i), i = setTimeout(function() {
            loadHeightFullWidthToSide(a), loadResponsiveMainMenu(a)
        }, 1100), clearTimeout(r), r = setTimeout(function() {
            row_equal_height_columns(a, !1)
        }, 1150), clearTimeout(d), d = setTimeout(function() {
            positionMenuMobile(a)
        }, 100)
    });
    var d = setTimeout(function() {
        positionMenuMobile(a)
    }, 100);
    if (a(".wide-nav .nasa-vertical-header").length > 0) {
        var c = a(".wide-nav .nasa-vertical-header").width();
        c = 280 > c ? 280 : c, a(".wide-nav .vertical-menu-container").css({
            width: c
        }), a(".wide-nav .vertical-menu-container").hasClass("nasa-allways-show") && a(".wide-nav .vertical-menu-container").addClass("nasa-active")
    }
    if (a(".progress-bar").length > 0 && a(".progress-bar").each(function() {
            var t = a(this).find(".bar-meter"),
                e = a(this).find(".bar-number"),
                n = a(t).attr("data-meter");
            a(this).waypoint(function() {
                a(t).css({
                    width: 0,
                    "max-width": n + "%"
                }), a(t).delay(50).animate({
                    width: n + "%"
                }, 400), a(e).delay(400).show(), setTimeout(function() {
                    a(e).css("opacity", 1)
                }, 400)
            }, {
                offset: n + "%",
                triggerOnce: !0
            })
        }), a(".collapses .collapses-title a").length > 0 && a("body").on("click", ".collapses .collapses-title a", function() {
            var t = a(this).parents(".collapses-group"),
                e = a(this).parents(".collapses");
            if (a(e).hasClass("active")) a(e).removeClass("active").find(".collapses-inner").slideUp(200);
            else {
                var n = a(t).find(".collapses.active");
                a(n).removeClass("active").find(".collapses-inner").slideUp(200), a(e).addClass("active").find(".collapses-inner").slideDown(200)
            }
            return !1
        }), a(".nasa-accordions-content .nasa-accordion-title a").length > 0 && (a(".nasa-accordions-content").each(function() {
            a(this).hasClass("nasa-accodion-first-hide") ? (a(this).find(".nasa-accordion.first").removeClass("active"), a(this).find(".nasa-panel.first").removeClass("active"), a(this).removeClass("nasa-accodion-first-hide")) : a(this).find(".nasa-panel.first.active").slideDown(200)
        }), a("body").on(_event, ".nasa-accordions-content .nasa-accordion-title a", function() {
            var t = a(this).parents(".nasa-accordions-content");
            a(t).removeClass("nasa-accodion-first-show");
            var e = a(this).attr("data-id");
            return a(this).hasClass("active") ? (a(this).removeClass("active"), a("#nasa-secion-" + e).removeClass("active").slideUp(200)) : (a(t).find(".nasa-accordion-title a").removeClass("active"), a(t).find(".nasa-panel.active").removeClass("active").slideUp(200), a("#nasa-secion-" + e).addClass("active").slideDown(200), a(this).addClass("active")), !1
        })), a(".nasa-tabs-content ul.nasa-tabs li a").length > 0 && (a("body").on(_event, ".nasa-tabs-content ul.nasa-tabs li a", function(t) {
            t.preventDefault();
            var e = a(this);
            if (!a(e).parent().hasClass("active")) {
                var n = a(e).parents(".nasa-tabs-content"),
                    s = a(e).attr("data-id"),
                    i = a(e).parent().attr("data-show");
                a(n).find("ul li").removeClass("active"), a(e).parent().addClass("active"), a(n).find("div.nasa-panel").removeClass("active").hide(), a(s).addClass("active").show(), a(s).parents(".vc_tta-panel").length && (a(s).parents(".nasa-panels").find(".vc_tta-panel").hide(), a(s).parents(".vc_tta-panel").show()), a(n).hasClass("nasa-slide-style") && nasa_tab_slide_style(a, n, 500);
                var r = a(s).find(".group-slider"),
                    o = a(s).find(".nasa-row-deal-3");
                wow_enable ? (a(s).find(".product-item").length > 0 || a(s).find(".product_list_widget").length > 0) && (a(s).css({
                    opacity: "0.9"
                }).append('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>'), a(n).find(".wow").css({
                    visibility: "hidden",
                    "animation-name": "none",
                    opacity: "0"
                }), a(r).length < 1 ? (a(s).find(".wow").removeClass("animated").css({
                    "animation-name": "fadeInUp"
                }), a(s).find(".wow").each(function() {
                    var t = a(this),
                        e = parseInt(a(t).attr("data-wow-delay"));
                    setTimeout(function() {
                        a(t).css({
                            visibility: "visible"
                        }), a(t).animate({
                            opacity: 1
                        }, e), a(s).find(".nasa-loader, .please-wait").length > 0 && (a(s).css({
                            opacity: 1
                        }), a(s).find(".nasa-loader, .please-wait").remove())
                    }, e)
                })) : (a(s).find(".owl-stage").css({
                    opacity: "0"
                }), setTimeout(function() {
                    a(s).find(".owl-stage").css({
                        opacity: "1"
                    })
                }, 500), a(s).find(".wow").each(function() {
                    var t = a(this);
                    a(t).css({
                        "animation-name": "fadeInUp",
                        visibility: "visible",
                        opacity: 0
                    });
                    var e = parseInt(a(t).attr("data-wow-delay"));
                    e += "0" === i ? 500 : 0, setTimeout(function() {
                        a(t).animate({
                            opacity: 1
                        }, e), a(s).find(".nasa-loader, .please-wait").length > 0 && (a(s).css({
                            opacity: 1
                        }), a(s).find(".nasa-loader, .please-wait").remove())
                    }, e)
                }))) : a(r).length > 0 && (a(s).css({
                    opacity: "0.9"
                }).append('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>'), a(s).find(".owl-stage").css({
                    opacity: "0"
                }), setTimeout(function() {
                    a(s).find(".owl-stage").css({
                        opacity: "1"
                    }), a(s).find(".nasa-loader, .please-wait").length > 0 && (a(s).css({
                        opacity: 1
                    }), a(s).find(".nasa-loader, .please-wait").remove())
                }, 300)), a(o).length > 0 && loadHeightDeal(a)
            }
            return !1
        }), a(".nasa-tabs-content.nasa-slide-style").length > 0 && (a(".nasa-slide-style").each(function() {
            var t = a(this);
            nasa_tab_slide_style(a, t, 500)
        }), a(window).resize(function() {
            a(".nasa-slide-style").each(function() {
                var t = a(this);
                nasa_tab_slide_style(a, t, 50)
            })
        }))), "undefined" != typeof nasa_countdown_l10n && ("undefined" == typeof p || "0" === p)) {
        var p = "1";
        a.countdown.regionalOptions[""] = {
            labels: [nasa_countdown_l10n.years, nasa_countdown_l10n.months, nasa_countdown_l10n.weeks, nasa_countdown_l10n.days, nasa_countdown_l10n.hours, nasa_countdown_l10n.minutes, nasa_countdown_l10n.seconds],
            labels1: [nasa_countdown_l10n.year, nasa_countdown_l10n.month, nasa_countdown_l10n.week, nasa_countdown_l10n.day, nasa_countdown_l10n.hour, nasa_countdown_l10n.minute, nasa_countdown_l10n.second],
            compactLabels: ["y", "m", "w", "d"],
            whichLabels: null,
            digits: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
            timeSeparator: ":",
            isRTL: !0
        }, a.countdown.setDefaults(a.countdown.regionalOptions[""]), loadCountDown(a)
    }
    /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) || (a(".yith-wcwl-wishlistexistsbrowse.show").each(function() {
        var t = a(this).find("a").text();
        a(this).find("a").attr("data-tip", t).addClass("tip-top")
    }), loadTipTop(a)), a(".nasa_banner .center").length > 0 && (a(".nasa_banner .center").vAlign(), a(window).resize(function() {
        a(".nasa_banner .center").vAlign()
    })), a(".col_hover_focus").length > 0 && a("body").on("hover", ".col_hover_focus", function() {
        a(this).parent().find(".columns > *").css("opacity", "0.5")
    }, function() {
        a(this).parent().find(".columns > *").css("opacity", "1")
    }), a(".add-to-cart-grid.product_type_simple").length > 0 && a("body").on("click", ".add-to-cart-grid.product_type_simple", function() {
        a(".mini-cart").addClass("active cart-active"), a(".mini-cart").hover(function() {
            a(".cart-active").removeClass("cart-active")
        }), setTimeout(function() {
            a(".cart-active").removeClass("active")
        }, 5e3)
    }), a(".row ~ br").remove(), a(".columns ~ br").remove(), a(".columns ~ p").remove(), a("select.ninja-forms-field,select.addon-select").wrap('<div class="custom select-wrapper"/>'), a(window).resize(), loadingCarousel(a), loadingSCCarosel(a), setInterval(function() {
        var t = a(".owl-carousel").data("owlCarousel");
        "undefined" != typeof t && t !== !1 && t.updateVars()
    }, 1500);
    var h = a.cookie("nasatheme_popup_closed");
    if ("do-not-show" !== h && a(".nasa-popup").length > 0 && a("body").hasClass("open-popup")) {
        var u = parseInt(a(".nasa-popup").attr("data-delay"));
        u = u ? 1e3 * u : 300;
        var m = "true" === a(".nasa-popup").attr("data-disable_mobile") ? !0 : !1;
        a(".nasa-popup").magnificPopup({
            items: {
                src: "#nasa-popup",
                type: "inline"
            },
            tClose: a('input[name="nasa-close-string"]').val(),
            removalDelay: 300,
            fixedContentPos: !0,
            callbacks: {
                beforeOpen: function() {
                    this.st.mainClass = "my-mfp-slide-bottom"
                },
                beforeClose: function() {
                    var t = a("#showagain:checked").val();
                    "do-not-show" === t && a.cookie("nasatheme_popup_closed", "do-not-show", {
                        expires: 7,
                        path: "/"
                    })
                }
            },
            disableOn: function() {
                return m && a(window).width() <= 640 ? !1 : !0
            }
        }), setTimeout(function() {
            a(".nasa-popup").magnificPopup("open")
        }, u), a("body").on("click", '#nasa-popup input[type="submit"]', function() {
            a(this).ajaxSuccess(function(t, e) {
                "object" == typeof e && "mail_sent" === e.responseJSON.status && (a("body").append('<div id="nasa-newsletter-alert" class="hidden-tag"><div class="wpcf7-response-output wpcf7-mail-sent-ok">' + e.responseJSON.message + "</div></div>"), a.cookie("nasatheme_popup_closed", "do-not-show", {
                    expires: 7,
                    path: "/"
                }), a.magnificPopup.close(), setTimeout(function() {
                    a("#nasa-newsletter-alert").fadeIn(300), setTimeout(function() {
                        a("#nasa-newsletter-alert").fadeOut(500)
                    }, 2e3)
                }, 300))
            })
        })
    }
    a("body").on("click", ".product-interactions .btn-compare", function() {
        var t = a(this);
        if (a(t).hasClass("nasa-compare")) {
            var e = a(t).attr("data-prod");
            e && add_compare_product(e, a)
        } else {
            var n = a(t).parents(".product-interactions");
            n.find(".compare-button .compare").trigger("click")
        }
        return !1
    }), a("body").on("click", ".nasa-remove-compare", function() {
        var t = a(this).attr("data-prod");
        return t && remove_compare_product(t, a), !1
    }), a("body").on("click", ".nasa-compare-clear-all", function() {
        return removeAll_compare_product(a), !1
    }), a("body").on("click", ".nasa-show-compare", function() {
        return a(this).hasClass("nasa-showed") ? hideCompare(a) : showCompare(a), !1
    }), a("body").on("click", ".product-interactions .btn-wishlist", function() {
        var t = a(this);
        if (!a(t).hasClass("nasa-disabled"))
            if (a(".btn-wishlist").addClass("nasa-disabled"), a(t).hasClass("nasa-added")) {
                var e = a(t).attr("data-prod");
                e && a("#yith-wcwl-row-" + e).length && a("#yith-wcwl-row-" + e).find(".nasa-remove_from_wishlist").length ? (a(t).removeClass("nasa-added"), a(t).addClass("nasa-unliked"), a("#yith-wcwl-row-" + e).find(".nasa-remove_from_wishlist").trigger("click"), setTimeout(function() {
                    a(t).removeClass("nasa-unliked")
                }, 1e3)) : a(".btn-wishlist").removeClass("nasa-disabled")
            } else {
                a(t).addClass("nasa-added");
                var n = a(t).parents(".product-interactions");
                n.find(".add_to_wishlist").trigger("click")
            }
        return !1
    });
    var f = !1,
        v = !1;
    if (a("body").on("click", ".btn-wishlist, .product-info .add_to_wishlist, .ajax_add_to_cart.btn-from-wishlist", function() {
            var t = a(this);
            v = !0, a(t).ajaxSuccess(function(t, e, n) {
                if (t = n = null, "object" == typeof e) {
                    var s = e.responseJSON;
                    if ("object" == typeof s && ("undefined" != typeof s.user_wishlists && "undefined" != typeof s.result || "undefined" != typeof s.fragments) && ("true" === s.result || "undefined" != typeof s.fragments) && a(".wishlist-number .nasa-sl").length > 0 && !f) {
                        var i = a('input[name="nasa-current-url"]').length > 0 ? a('input[name="nasa-current-url"]').val() : "";
                        f = !0, a.ajax({
                            url: ajaxurl,
                            type: "post",
                            dataType: "json",
                            data: {
                                action: "nasa_update_wishlist",
                                nasa_current_url: i
                            },
                            beforeSend: function() {},
                            success: function(t) {
                                a(".wishlist_sidebar").replaceWith(t.list);
                                var e = t.count.toString().replace("+", ""),
                                    n = parseInt(e);
                                a(".wishlist-number .nasa-sl").html(t.count), n > 0 ? a(".wishlist-number").removeClass("nasa-product-empty") : 0 !== n || a(".wishlist-number").hasClass("nasa-product-empty") || a(".wishlist-number").addClass("nasa-product-empty")
                            }
                        })
                    }
                }
            }).ajaxComplete(function() {
                initWishlistIcons(a), f = v = !1, t = null
            })
        }), a("body").on("click", ".nasa-remove_from_wishlist", function() {
            var t = a(this).attr("data-prod_id"),
                e = a(".wishlist_table").attr("data-id"),
                n = a(".wishlist_table").attr("data-pagination"),
                s = a(".wishlist_table").attr("data-per-page"),
                i = a(".wishlist_table").attr("data-page");
            return a.ajax({
                url: ajaxurl,
                type: "post",
                dataType: "json",
                data: {
                    action: "nasa_remove_from_wishlist",
                    pid: t,
                    wishlist_id: e,
                    pagination: n,
                    per_page: s,
                    current_page: i
                },
                beforeSend: function() {
                    a.magnificPopup.close(), a(".nasa-wishlist-fog").show().html('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>')
                },
                success: function(e) {
                    if ("0" === e.error) {
                        a(".nasa-wishlist-fog").hide(), a(".wishlist_sidebar").replaceWith(e.list);
                        var n = e.count.toString().replace("+", ""),
                            s = parseInt(n);
                        a(".wishlist-number .nasa-sl").html(e.count), s > 0 ? a(".wishlist-number").removeClass("nasa-product-empty") : 0 !== s || a(".wishlist-number").hasClass("nasa-product-empty") || (a(".wishlist-number").addClass("nasa-product-empty"), a(".wishlist-close a").click()), a('.btn-wishlist[data-prod="' + t + '"]').length && (a('.btn-wishlist[data-prod="' + t + '"]').removeClass("nasa-added"), a('.btn-wishlist[data-prod="' + t + '"]').find(".added").length && a('.btn-wishlist[data-prod="' + t + '"]').find(".added").removeClass("added"))
                    }
                }
            }), !1
        }), a(".compare-number").length > 0 && a("body").on("click", ".btn-compare, .yith-woocompare-widget .compare", function() {
            f = !0, a(this).ajaxSuccess(function(t, e, n) {
                if (t = n = null, a(".compare-number .nasa-sl").length > 0 && !v) {
                    v = !0;
                    var s, i, r = !1;
                    if ("object" == typeof e) {
                        if (s = e.responseText.substring(0, 1), "{" === s) {
                            var o = e.responseJSON;
                            "undefined" != typeof o.widget_table ? (i = "<ul>" + JSON.parse(e.responseText).widget_table + "</ul>", r = !0) : r = !1
                        } else s = e.responseText.trim().substring(0, 4), "<li>" === s ? (i = "<li>No products to compare</li>" === e.responseText ? "<ul></ul>" : "<ul>" + e.responseText + "</ul>", r = !0) : r = !1;
                        if (r) {
                            var l = a(i).find("li").length;
                            l = l ? l : 0, a(".compare-number .nasa-sl").html(l), l > 0 ? a(".compare-number").removeClass("nasa-product-empty") : 0 != l || a(".compare-number").hasClass("nasa-product-empty") || a(".compare-number").addClass("nasa-product-empty")
                        }
                    }
                }
            }).ajaxComplete(function() {
                v = f = !1
            }).ajaxError(function() {
                v = f = !1
            })
        }), a("body").find("input.qty:not(.product-quantity input.qty)").each(function() {
            var t = parseFloat(a(this).attr("min"));
            t && t > 0 && parseFloat(a(this).val()) < t && a(this).val(t)
        }), a("body").on("click", ".cart .plus, .cart .minus", function() {
            var t = a(this).parents(".quantity").find(".qty"),
                e = a(this).parent().parent().find(".single_add_to_cart_button"),
                n = parseFloat(t.val()),
                s = parseFloat(t.attr("max")),
                i = parseFloat(t.attr("min")),
                r = t.attr("step");
            n = n ? n : 0, s = s ? s : "", i = i ? i : 1, ("any" === r || "" === r || "undefined" == typeof r || "NaN" === parseFloat(r)) && (r = 1), a(this).is(".plus") ? s && (s == n || n > s) ? (t.val(s), e.length > 0 && e.attr("data-quantity", s)) : (t.val(n + parseFloat(r)), e.length > 0 && e.attr("data-quantity", n + parseFloat(r))) : i && (i == n || i > n) ? (t.val(i), e.length > 0 && e.attr("data-quantity", i)) : n > 0 && (t.val(n - parseFloat(r)), e.length > 0 && e.attr("data-quantity", n - parseFloat(r))), t.trigger("change")
        }), "undefined" != typeof search_options.enable_live_search && "1" == search_options.enable_live_search) {
        var g = a("#nasa-empty-result-search").html(),
            w = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace("title"),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: ajaxurl + "?action=live_search_products",
                limit: search_options.limit_results,
                remote: {
                    url: ajaxurl + "?action=live_search_products&s=%QUERY",
                    ajax: {
                        data: {
                            cat: a(".nasa-cats-search").val()
                        },
                        beforeSend: function() {
                            0 === a(".live-search-input").parent().find(".loader-search").length && a(".live-search-input").parent().append('<div class="nasa-loader nasa-live-search-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>')
                        },
                        success: function() {
                            a(".nasa-live-search-loader").remove()
                        },
                        error: function() {
                            a(".nasa-live-search-loader").remove()
                        }
                    }
                }
            }),
            b = !1;
        a("body").on("focus", "input.live-search-input", function() {
            b || (w.initialize(), b = !0)
        }), a(".live-search-input").typeahead({
            minLength: 3,
            hint: !0,
            highlight: !1,
            backdrop: {
                opacity: .8,
                filter: "alpha(opacity=80)",
                "background-color": "#eaf3ff"
            },
            backdropOnFocus: !0,
            callback: {
                onInit: function() {
                    w.initialize()
                },
                onSubmit: function(a, t) {
                    t.submit()
                }
            }
        }, {
            name: "search",
            source: w.ttAdapter(),
            displayKey: "title",
            templates: {
                empty: '<p class="empty-message" style="padding:0;margin:0;font-size:100%;">' + g + "</p>",
                suggestion: Handlebars.compile(search_options.live_search_template)
            }
        })
    }
    var _ = a(window).width();
    if (a(window).resize(function() {
            _ = a(window).width(), 768 >= _ && a(".hover-lax").css("background-position", "center center")
        }), a("body").on("mousemove", ".hover-lax", function(t) {
            var e = a(this),
                n = a(e).attr("data-minwidth") ? a(e).attr("data-minwidth") : 768;
            if (_ > n) {
                var s = -1 * t.pageX / 6,
                    i = -1 * t.pageY / 6;
                a(e).css("background-position", s + "px " + i + "px")
            } else a(e).css("background-position", "center center")
        }), a("body").on("click", ".mobile-search", function() {
            a(".black-window").fadeIn(200);
            var t = a("#wpadminbar").length > 0 ? a("#wpadminbar").height() : 0;
            a(".warpper-mobile-search").show().animate({
                top: t
            }, 700), a(".warpper-mobile-search").find('input[name="s"]').val("").focus()
        }), a("body").on("click", ".desk-search", function(t) {
            var e = a(this);
            if (!a(e).hasClass("nasa-disable")) {
                a(e).addClass("nasa-disable");
                var n = a(e).parents(".nasa-wrap-event-search").find(".nasa-show-search-form"),
                    s = a(e).attr("data-open");
                "0" === s ? a(".header-wrapper").find(".nasa-show-search-form").after('<div class="nasa-tranparent" />') : a(".header-wrapper").find(".nasa-tranparent").remove(), a(".desk-search").each(function() {
                    var t = a(this),
                        e = a(t).parents(".nasa-wrap-event-search"),
                        n = a(e).find(".nasa-elements-wrap"),
                        i = a(e).find(".nasa-show-search-form");
                    "undefined" == typeof s || "0" === s ? (a(t).attr("data-open", "1"), a(i).hasClass("nasa-show") || a(i).addClass("nasa-show"), a(n).addClass("nasa-invisible")) : (a(t).attr("data-open", "0"), a(i).hasClass("nasa-show") && a(i).removeClass("nasa-show"), a(n).removeClass("nasa-invisible"))
                }), setTimeout(function() {
                    a(e).removeClass("nasa-disable"), a(n).find('input[name="s"]').focus().val("")
                }, 1e3)
            }
            t.preventDefault()
        }), a("body").on("click", ".nasa-close-search, .nasa-tranparent", function() {
            a(this).parents(".nasa-wrap-event-search").find(".desk-search").click()
        }), a("body").on("click", ".toggle-sidebar-shop", function() {
            a(".transparent-window").fadeIn(200), a(".nasa-side-sidebar").hasClass("nasa-show") || a(".nasa-side-sidebar").addClass("nasa-show")
        }), a("body").on("click", ".toggle-sidebar", function() {
            a(".black-window").fadeIn(200), a(".col-sidebar").hasClass("left") ? a(".col-sidebar").show().animate({
                left: 0
            }, 700) : a(".col-sidebar").show().animate({
                right: 0
            }, 700)
        }), 1 === a('input[name="nasa_cart_sidebar_show"]').length && "1" === a('input[name="nasa_cart_sidebar_show"]').val() && setTimeout(function() {
            a(".cart-link").click()
        }, 300), a("body").on("click", ".cart-link", function() {
            a(".black-window").fadeIn(200).addClass("desk-window"), a("#cart-sidebar").show().addClass("nasa-active").animate({
                right: 0
            }, 700)
        }), a("body").on("click", ".wishlist-link", function() {
            a(this).hasClass("wishlist-link-premium") || (a(".black-window").fadeIn(200).addClass("desk-window"), a("#nasa-wishlist-sidebar").show().addClass("nasa-active").animate({
                right: 0
            }, 700))
        }), a("body").on("click", "#nasa-init-viewed", function() {
            a(".black-window").fadeIn(200).addClass("desk-window"), a("#nasa-viewed-sidebar").show().animate({
                right: 0
            }, 1e3)
        }), a("body").on("click", ".black-window, .white-window, .transparent-mobile, .transparent-window, .nasa-close-mini-compare, .nasa-sidebar-close a, .nasa-sidebar-return-shop, .login-register-close a", function() {
            var t = a(".nasa-check-reponsive.nasa-switch-check").length && 1 === a(".nasa-check-reponsive.nasa-switch-check").width() ? !0 : !1;
            if (a(".black-window").hasClass("desk-window") && a(".black-window").removeClass("desk-window"), 1 === a("#mobile-navigation").length && "1" === a("#mobile-navigation").attr("data-show")) {
                var e = a("#nasa-menu-sidebar-content").outerWidth().toString();
                a("#nasa-menu-sidebar-content").animate({
                    left: "-" + e + "px"
                }, 800), a("#mobile-navigation").attr("data-show", "0"), setTimeout(function() {
                    a(".black-window").removeClass("nasa-transparent"), a("#nasa-menu-sidebar-content").css({
                        "z-index": 399
                    })
                }, 800)
            }
            a(".warpper-mobile-search").length > 0 && (a(".warpper-mobile-search").animate({
                top: "-100%"
            }, 1e3), a(".warpper-mobile-search").hasClass("show-in-desk") && setTimeout(function() {
                a(".warpper-mobile-search").removeClass("show-in-desk")
            }, 600)), a(".col-sidebar").length > 0 && t && (a(".col-sidebar").hasClass("left") ? a(".col-sidebar").animate({
                left: "-100%"
            }, 700) : a(".col-sidebar").animate({
                right: "-100%"
            }, 700)), a("#cart-sidebar").length > 0 && a("#cart-sidebar").removeClass("nasa-active").animate({
                right: "-100%"
            }, 700), a("#nasa-wishlist-sidebar").length > 0 && a("#nasa-wishlist-sidebar").removeClass("nasa-active").animate({
                right: "-100%"
            }, 700), a("#nasa-viewed-sidebar").length > 0 && a("#nasa-viewed-sidebar").animate({
                right: "-100%"
            }, 700), a(".nasa-login-register-warper").length > 0 && a(".nasa-login-register-warper").animate({
                top: "-1000px"
            }, 700), a("#nasa-quickview-sidebar").length > 0 && a("#nasa-quickview-sidebar").removeClass("nasa-active").animate({
                right: "-100%"
            }, 1500), a(".nasa-top-cat-filter-wrap-mobile").length > 0 && a(".nasa-top-cat-filter-wrap-mobile").removeClass("nasa-show"), a(".nasa-side-sidebar").length > 0 && a(".nasa-side-sidebar").removeClass("nasa-show"), hideCompare(a), a(".black-window, .white-window, .transparent-mobile, .transparent-window").fadeOut(1e3)
        }), a(document).on("keyup", function(t) {
            27 === t.keyCode && (a(".nasa-tranparent").click(), a(".nasa-tranparent-filter").click(), a(".black-window, .white-window, .transparent-mobile, .transparent-window, .nasa-close-mini-compare, .nasa-sidebar-close a, .login-register-close a, .nasa-transparent-topbar, .nasa-close-filter-cat").click(), a.magnificPopup.close())
        }), a("body").on("click", ".add_to_cart_button", function() {
            if (a(this).hasClass("product_type_simple")) a.magnificPopup.close(), setTimeout(function() {
                a(".black-window").fadeIn(200).addClass("desk-window"), a("#nasa-wishlist-sidebar").removeClass("nasa-active").animate({
                    right: "-100%"
                }, 700), a("#cart-sidebar").show().addClass("nasa-active").animate({
                    right: 0
                }, 700)
            }, 200);
            else {
                var t = a(this).attr("href");
                window.location.href = t
            }
        }), a(".nasa-cart-fog").length > 0 && a("body").ajaxSend(function() {
            (a("#cart-sidebar").hasClass("nasa-active") || a("#nasa-wishlist-sidebar").hasClass("nasa-active")) && a(".nasa-cart-fog").show().html('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>')
        }).ajaxComplete(function() {
            a(".nasa-cart-fog").hide()
        }).ajaxError(function() {
            a(".nasa-cart-fog").hide()
        }), a("body").on("click", '.nasa-shopping-cart-form .woocommerce-cart-form__cart-item .product-remove .remove, .nasa-shopping-cart-form input[name="update_cart"]', function() {
            var t = a(this);
            a(t).addClass("nasa-loading"), a(document).ajaxSuccess(function() {
                a(t).hasClass("nasa-loading") && a.ajax({
                    url: ajaxurl,
                    type: "post",
                    dataType: "json",
                    data: {
                        action: "nasa_cart_refress"
                    },
                    beforeSend: function() {},
                    success: function(e) {
                        "undefined" != typeof e && a.each(e, function(t, e) {
                            a(t).length > 0 && a(t).replaceWith(e)
                        }), a(t).removeClass("nasa-loading")
                    },
                    error: function() {
                        a(t).removeClass("nasa-loading")
                    }
                })
            })
        }), a("body").on("click", ".remove.item-in-cart", function() {
            var t = a(this),
                e = a(t).attr("data-key"),
                n = a(t).attr("data-id");
            a(".remove.item-in-cart").removeClass("remove"), e && n && (a('form.nasa-shopping-cart-form .woocommerce-cart-form__contents .product-remove a.remove[data-product_id="' + n + '"]').length > 0 ? a('form.nasa-shopping-cart-form .woocommerce-cart-form__contents .product-remove a.remove[data-product_id="' + n + '"]').click() : a.ajax({
                url: ajaxurl,
                type: "post",
                dataType: "json",
                data: {
                    action: "nasa_cart_remove_item",
                    item_key: e
                },
                beforeSend: function() {},
                success: function(t) {
                    if (t.success) {
                        var e = t.fragments;
                        if (e && (a.each(e, function(t, e) {
                                a(t).addClass("updating"), a(t).replaceWith(e)
                            }), a(".nasa-cart-fog").hide()), a('.add_to_cart_button[data-product_id="' + n + '"]').length && (a('.add_to_cart_button[data-product_id="' + n + '"]').removeClass("added"), a('.add_to_cart_button[data-product_id="' + n + '"]').removeClass("nasa-added")), t.showing < 1) {
                            var s = a(".empty.hidden-tag").html();
                            a(".cart_sidebar").html(s), setTimeout(function() {
                                a(".black-window").removeClass("desk-window"), a("#cart-sidebar").removeClass("nasa-active").animate({
                                    right: "-100%"
                                }, 700), a(".black-window").fadeOut(200)
                            }, 200)
                        }
                    }
                },
                error: function() {
                    a("#cart-sidebar .item-in-cart").addClass("remove")
                }
            }))
        }), a("body").on("click", ".nasa_add_to_cart_from_wishlist", function() {
            var t = a(this),
                e = a(t).attr("data-product_id");
            if (e) {
                var n = a(t).attr("data-type"),
                    s = a(t).attr("data-quantity"),
                    i = {};
                a(".wishlist_table").length > 0 && a(".wishlist_table").find("tr#yith-wcwl-row-" + e).length > 0 && (i = {
                    from_wishlist: "1",
                    wishlist_id: a(".wishlist_table").attr("data-id"),
                    pagination: a(".wishlist_table").attr("data-pagination"),
                    per_page: a(".wishlist_table").attr("data-per-page"),
                    current_page: a(".wishlist_table").attr("data-page")
                }), nasa_single_add_to_cart(a, t, e, s, n, null, null, i)
            }
            return !1
        }), a("body").on("click", "form.cart .single_add_to_cart_button", function() {
            var t = !0,
                e = a(this),
                n = a(e).parents("form.cart"),
                s = a(n).find('input[name="nasa-enable-addtocart-ajax"]');
            if (a(s).length <= 0 || "1" !== a(s).val()) return t = !1, void 0;
            var i = a(e).hasClass("disabled") ? !1 : a(n).find('input[name="data-product_id"]').val();
            if (i) {
                var r = a(n).find('input[name="data-type"]').val(),
                    o = a(n).find('.quantity input[name="quantity"]').val(),
                    l = a(n).find('input[name="variation_id"]').length > 0 ? parseInt(a(n).find('input[name="variation_id"]').val()) : 0,
                    d = {},
                    c = {},
                    p = 1 === a(n).find('input[name="data-from_wishlist"]').length && "1" === a(n).find('input[name="data-from_wishlist"]').val() ? "1" : "0";
                if ("variable" === r && !l) return t = !1, !1;
                l > 0 && a(n).find(".variations").length > 0 && (a(n).find(".variations").find("select").each(function() {
                    d[a(this).attr("name")] = a(this).val()
                }), a(".wishlist_table").length > 0 && a(".wishlist_table").find("tr#yith-wcwl-row-" + i).length > 0 && (c = {
                    from_wishlist: p,
                    wishlist_id: a(".wishlist_table").attr("data-id"),
                    pagination: a(".wishlist_table").attr("data-pagination"),
                    per_page: a(".wishlist_table").attr("data-per-page"),
                    current_page: a(".wishlist_table").attr("data-page")
                })), t && nasa_single_add_to_cart(a, e, i, o, r, l, d, c)
            }
            return !1
        }), a("body").on("click", ".nasa_bundle_add_to_cart", function() {
            var t = a(this),
                e = a(t).attr("data-product_id");
            if (e) {
                var n = a(t).attr("data-type"),
                    s = a(t).attr("data-quantity"),
                    i = 0,
                    r = {},
                    o = {};
                nasa_single_add_to_cart(a, t, e, s, n, i, r, o)
            }
            return !1
        }), a("body").on("click", ".product_type_variation.add-to-cart-grid", function() {
            var t = a(this);
            if (!a(t).hasClass("nasa-disable-ajax")) {
                var e = a(t).attr("data-product_id");
                if (e) {
                    var n = "variation",
                        s = a(t).attr("data-quantity"),
                        i = 0,
                        r = null,
                        o = {};
                    "undefined" != typeof a(this).attr("data-variation") && (r = JSON.parse(a(this).attr("data-variation"))), r && nasa_single_add_to_cart(a, t, e, s, n, i, r, o)
                }
                return !1
            }
        }), a("body").on("click", ".product_type_variable", function() {
            if (a('input[name="nasa-disable-quickview-ux"]').length <= 0 || "0" === a('input[name="nasa-disable-quickview-ux"]').val()) {
                var t = a(this);
                if (a(t).parents(".compare-list").length > 0) return;
                if (a(t).hasClass("btn-from-wishlist")) {
                    var e = a(t).parents(".add-to-cart-wishlist"),
                        n = a(t).parents(".product-wishlist-info").find(".wishlist-item");
                    a(n).css({
                        opacity: .3
                    }), a(n).after('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>'), a(e).find(".quick-view").click()
                } else {
                    var e = a(t).parents(".product-interactions");
                    a(e).length < 1 && (e = a(t).parents(".item-product-widget")), a(e).find(".quick-view").click()
                }
                return !1
            }
        }), a(".nasa-post-slider").length > 0) {
        var y = parseInt(a(".nasa-post-slider").attr("data-show"));
        a(".nasa-post-slider").owlCarousel({
            items: y,
            loop: !0,
            nav: !1,
            autoplay: !0,
            dots: !1,
            autoHeight: !0,
            autoplayTimeout: 3e3,
            autoplayHoverPause: !0,
            responsiveClass: !0,
            navText: ["", ""],
            navSpeed: 600,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                600: {
                    items: 1,
                    nav: !1
                },
                1000: {
                    items: y,
                    nav: !1
                }
            }
        })
    }
    if (a(".nasa-promotion-close").length > 0) {
        var C = a(".nasa-promotion-news").outerHeight();
        "hide" !== a.cookie("promotion") && (a(".nasa-position-relative").animate({
            height: C + "px"
        }, 500), a(".nasa-promotion-news").fadeIn(500)), a("body").on("click", ".nasa-promotion-close", function() {
            a.cookie("promotion", "hide", {
                expires: 7,
                path: "/"
            }), a(".nasa-promotion-show").show(), a(".nasa-position-relative").animate({
                height: "0px"
            }, 500), a(".nasa-promotion-news").fadeOut(500)
        }), a("body").on("click", ".nasa-promotion-show", function() {
            a.cookie("promotion", "show", {
                path: "/"
            }), a(".nasa-promotion-show").hide(), a(".nasa-position-relative").animate({
                height: C + "px"
            }, 500), a(".nasa-promotion-news").fadeIn(500)
        })
    }
    var k = 0,
        x = 0,
        T = "0";
    if (a(".price_slider_wrapper").length > 0 && (a(".price_slider_wrapper").find("input").attr("readonly", !0), k = parseFloat(a(".price_slider_wrapper").find('input[name="min_price"]').val()), x = parseFloat(a(".price_slider_wrapper").find('input[name="max_price"]').val()), T = a(".nasa_hasPrice").length > 0 ? a(".nasa_hasPrice").val() : "0", "1" === T && (a(".reset_price").attr("data-has_price", "1").show(), a(".price_slider_wrapper").find("button").length && a(".price_slider_wrapper").find("button").show())), a("body").on("click", ".price_slider_wrapper button", function(t) {
            if (t.preventDefault(), "1" === T && a(".nasa-has-filter-ajax").length < 1) {
                var e = a(this).parents("form");
                a('input[name="nasa_hasPrice"]').remove(), a(e).submit()
            }
        }), a("body").on("slidestop", ".price_slider", function() {
            var t = a(this).parents("form");
            if (!(a(".nasa-has-filter-ajax").length < 1)) {
                var e = parseFloat(a(t).find('input[name="min_price"]').val()),
                    n = parseFloat(a(t).find('input[name="max_price"]').val());
                if (0 > e && (e = 0), e > n && (n = e), e != k || n != x) {
                    a(t).find("button").length && a(t).find("button").show(), k = e, x = n, T = "1", a(".nasa_hasPrice").length > 0 && (a(".nasa_hasPrice").val("1"), a(".reset_price").attr("data-has_price", "1").fadeIn(200));
                    var s = a(".current-cat > .nasa-filter-by-cat"),
                        i = a('select[name="orderby"]').val(),
                        r = !1,
                        o = null,
                        l = "",
                        d = "";
                    a(s).length > 0 && (o = a(s).attr("data-id"), l = a(s).attr("data-taxonomy"), d = a(s).attr("href"));
                    var c = nasa_setVariations(a, [], []),
                        p = a("input#nasa_hasSearch").length > 0 && "" !== a("input#nasa_hasSearch").val() ? 1 : 0,
                        h = 1 === p ? a("input#nasa_hasSearch").val() : "";
                    a(t).find("button").length ? a(t).find("button").click(function() {
                        nasa_Ajax_filter(a, d, r, o, i, c, T, e, n, p, h, s, l)
                    }) : nasa_Ajax_filter(a, d, r, o, i, c, T, e, n, p, h, s, l)
                }
                return !1
            }
            a(t).find("button").length ? (a(t).find("button").show(), a(t).find(".nasa_hasPrice").length > 0 && (a(t).find(".nasa_hasPrice").val("1"), a(t).find(".reset_price").attr("data-has_price", "1").fadeIn(200)), a(t).find("button").click(function() {
                a('input[name="nasa_hasPrice"]').remove(), a(t).submit()
            })) : a(t).submit()
        }), a("body").on("click", ".reset_price", function() {
            if (a(".nasa_hasPrice").length > 0 && "1" === a(".nasa_hasPrice").val()) {
                var t = a(this).parents("form");
                if (a(".nasa-has-filter-ajax").length < 1) a("#min_price").remove(), a("#max_price").remove(), a('input[name="nasa_hasPrice"]').remove(), a(t).append('<input type="hidden" name="reset-price" value="true" />'), a(t).submit();
                else if (!shop_load) {
                    shop_load = !0;
                    var e = a("#min_price").attr("data-min"),
                        n = a("#max_price").attr("data-max");
                    a(".price_slider").slider("values", 0, e), a(".price_slider").slider("values", 1, n), a("#min_price").val(e), a("#max_price").val(n);
                    var s = a('input[name="nasa_currency_pos"]').val(),
                        i = e,
                        r = n;
                    switch (s) {
                        case "left":
                            i = woocommerce_price_slider_params.currency_format_symbol + e, r = woocommerce_price_slider_params.currency_format_symbol + n;
                            break;
                        case "right":
                            i = e + woocommerce_price_slider_params.currency_format_symbol, r = n + woocommerce_price_slider_params.currency_format_symbol;
                            break;
                        case "left_space":
                            i = woocommerce_price_slider_params.currency_format_symbol + " " + e, r = woocommerce_price_slider_params.currency_format_symbol + " " + n;
                            break;
                        case "right_space":
                            i = e + " " + woocommerce_price_slider_params.currency_format_symbol, r = n + " " + woocommerce_price_slider_params.currency_format_symbol
                    }
                    a(".price_slider_amount .price_label span.from").html(i), a(".price_slider_amount .price_label span.to").html(r);
                    var o = 0,
                        l = 0;
                    T = "0", a(".nasa_hasPrice").length > 0 && (a(".nasa_hasPrice").val("0"), a(".reset_price").attr("data-has_price", "0").fadeOut(200));
                    var d = a(".current-cat > .nasa-filter-by-cat"),
                        c = a('select[name="orderby"]').val(),
                        p = !1,
                        h = null,
                        u = "",
                        m = "";
                    a(d).length > 0 && (h = a(d).attr("data-id"), u = a(d).attr("data-taxonomy"), m = a(d).attr("href"));
                    var f = nasa_setVariations(a, [], []),
                        v = a("input#nasa_hasSearch").length > 0 && "" !== a("input#nasa_hasSearch").val() ? 1 : 0,
                        g = 1 === v ? a("input#nasa_hasSearch").val() : "";
                    nasa_Ajax_filter(a, m, p, h, c, f, T, o, l, v, g, d, u)
                }
                return !1
            }
        }), a("body").on("click", ".nasa-filter-by-price-list", function() {
            if (!(a(".nasa-has-filter-ajax").length < 1)) {
                if (!shop_load) {
                    shop_load = !0;
                    var t = a(this).attr("href"),
                        e = a(this).attr("data-min") ? a(this).attr("data-min") : null,
                        n = a(this).attr("data-max") ? a(this).attr("data-max") : null;
                    0 > e && (e = 0), e > n && (n = e), (e != k || n != x) && (T = "1");
                    var s = a(".current-cat > .nasa-filter-by-cat"),
                        i = a('select[name="orderby"]').val(),
                        r = !1,
                        o = null,
                        l = "";
                    a(s).length > 0 && (o = a(s).attr("data-id"), l = a(s).attr("data-taxonomy"));
                    var d = [],
                        c = a("input#nasa_hasSearch").val(),
                        p = c ? 1 : 0;
                    nasa_Ajax_filter(a, t, r, o, i, d, T, e, n, p, c, s, l, !1, !1, !1)
                }
                return !1
            }
        }), renderTagClouds(a), a("body").on("click", ".nasa-reset-filters-btn", function() {
            if (!(a(".nasa-has-filter-ajax").length < 1)) {
                if (!shop_load) {
                    shop_load = !0;
                    var t = a(this),
                        e = a(t).attr("data-id"),
                        n = a(t).attr("data-taxonomy"),
                        s = !1,
                        i = a(t).attr("href"),
                        r = !1,
                        o = [],
                        l = null,
                        d = null;
                    a("input#nasa_hasSearch").val(""), T = "0", nasa_Ajax_filter(a, i, r, e, s, o, T, l, d, 0, "", t, n)
                }
                return !1
            }
        }), a("body").on("click", ".nasa-filter-by-cat", function() {
            if (!(a(".nasa-has-filter-ajax").length < 1)) {
                if (!shop_load && !a(this).hasClass("nasa-disable") && !a(this).hasClass("nasa-active")) {
                    shop_load = !0, a("li.cat-item").removeClass("current-cat");
                    var t = a(this),
                        e = a(t).attr("data-id"),
                        n = a(t).attr("data-taxonomy"),
                        s = a('select[name="orderby"]').val(),
                        i = a(t).attr("href"),
                        r = !1;
                    if (e) {
                        var o = [];
                        a(".nasa-filter-by-variations").each(function() {
                            a(this).hasClass("nasa-filter-var-chosen") && (a(this).parent().removeClass("chosen nasa-chosen"), a(this).removeClass("nasa-filter-var-chosen"))
                        });
                        var l = null,
                            d = null;
                        a("input#nasa_hasSearch").val(""), T = "0", a(".black-window-mobile.nasa-push-cat-show").width() && a(".black-window-mobile.nasa-push-cat-show").click(), nasa_Ajax_filter(a, i, r, e, s, o, T, l, d, 0, "", t, n), 1 === a(t).parents(".nasa-filter-cat-no-top-icon").length && a(".nasa-tab-filter-topbar-categories").length > 0 && a(".nasa-tab-filter-topbar-categories").click()
                    }
                }
                return !1
            }
        }), a(".woocommerce-ordering").length > 0 && a(".nasa-has-filter-ajax").length > 0) {
        var j = a(".woocommerce-ordering").parent(),
            S = a(".woocommerce-ordering").html();
        a(j).html(S)
    }
    a("body").on("change", 'select[name="orderby"]', function() {
        if (a(".nasa-has-filter-ajax").length <= 0) return a(this).parents("form").submit(), void 0;
        if (!shop_load) {
            shop_load = !0;
            var t = a(".current-cat > .nasa-filter-by-cat"),
                e = a(this).val(),
                n = !1,
                s = null,
                i = "",
                r = "";
            a(t).length > 0 && (s = a(t).attr("data-id"), i = a(t).attr("data-taxonomy"), r = a(t).attr("href"));
            var o = nasa_setVariations(a, [], []),
                l = null,
                d = null;
            if ("1" === T) {
                var c = a(".price_slider").parents("form");
                a(c).length > 0 && (l = parseFloat(a(c).find('input[name="min_price"]').val()), d = parseFloat(a(c).find('input[name="max_price"]').val()), 0 > l && (l = 0), l > d && (d = l))
            }
            var p = a("input#nasa_hasSearch").length > 0 && "" !== a("input#nasa_hasSearch").val() ? 1 : 0,
                h = 1 === p ? a("input#nasa_hasSearch").val() : "";
            nasa_Ajax_filter(a, r, n, s, e, o, T, l, d, p, h, t, i)
        }
        return !1
    }), a("body").on("click", ".nasa-pagination-ajax .page-numbers", function() {
        if (!a(this).hasClass("nasa-current")) {
            if (!shop_load) {
                shop_load = !0;
                var t = a(".current-cat > .nasa-filter-by-cat"),
                    e = a('select[name="orderby"]').val(),
                    n = a(this).attr("data-page"),
                    s = null,
                    i = "",
                    r = "";
                "1" === n && (n = !1), a(t).length > 0 && (s = a(t).attr("data-id"), i = a(t).attr("data-taxonomy"), r = a(t).attr("href"));
                var o = nasa_setVariations(a, [], []),
                    l = null,
                    d = null;
                if ("1" === T) {
                    var c = a(".price_slider").parents("form");
                    a(c).length > 0 && (l = parseFloat(a(c).find('input[name="min_price"]').val()), d = parseFloat(a(c).find('input[name="max_price"]').val()), 0 > l && (l = 0), l > d && (d = l))
                }
                var p = a("input#nasa_hasSearch").length > 0 && "" !== a("input#nasa_hasSearch").val() ? 1 : 0,
                    h = 1 === p ? a("input#nasa_hasSearch").val() : "";
                nasa_Ajax_filter(a, r, n, s, e, o, T, l, d, p, h, t, i, !0)
            }
            return !1
        }
    }), a("body").on("click", ".nasa-archive-loadmore", function() {
        if (!(a(".nasa-has-filter-ajax").length < 1)) {
            if (!shop_load) {
                shop_load = !0, a(this).addClass("nasa-disabled"), archive_page += 1;
                var t = a(".current-cat > .nasa-filter-by-cat"),
                    e = a('select[name="orderby"]').val(),
                    n = archive_page,
                    s = null,
                    i = "",
                    r = "";
                1 == n && (n = !1), a(t).length > 0 && (s = a(t).attr("data-id"), i = a(t).attr("data-taxonomy"), r = a(t).attr("href"));
                var o = nasa_setVariations(a, [], []),
                    l = null,
                    d = null;
                if ("1" === T) {
                    var c = a(".price_slider").parents("form");
                    a(c).length > 0 && (l = parseFloat(a(c).find('input[name="min_price"]').val()), d = parseFloat(a(c).find('input[name="max_price"]').val()), 0 > l && (l = 0), l > d && (d = l))
                }
                var p = a("input#nasa_hasSearch").length > 0 && "" !== a("input#nasa_hasSearch").val() ? 1 : 0,
                    h = 1 === p ? a("input#nasa_hasSearch").val() : "";
                nasa_Ajax_filter(a, r, n, s, e, o, T, l, d, p, h, t, i, !1, !0)
            }
            return !1
        }
    }), a("body").on("click", ".nasa-filter-by-variations", function() {
        if (!(a(".nasa-has-filter-ajax").length < 1)) {
            if (!shop_load) {
                shop_load = !0;
                var t = a(".current-cat > .nasa-filter-by-cat"),
                    e = a('select[name="orderby"]').val(),
                    n = !1,
                    s = null,
                    i = "",
                    r = "";
                a(t).length > 0 && (s = a(t).attr("data-id"), i = a(t).attr("data-taxonomy"), r = a(t).attr("href"));
                var o = [],
                    l = [],
                    d = !1;
                a(this).hasClass("nasa-filter-var-chosen") ? (a(this).parent().removeClass("chosen nasa-chosen").show(), a(this).removeClass("nasa-filter-var-chosen")) : (a(this).parent().addClass("chosen nasa-chosen"), a(this).addClass("nasa-filter-var-chosen")), d = !0, d && (o = nasa_setVariations(a, o, l));
                var c = null,
                    p = null,
                    h = a(".price_slider").parents("form");
                a(h).length > 0 && "1" === T && (c = parseFloat(a(h).find('input[name="min_price"]').val()), p = parseFloat(a(h).find('input[name="max_price"]').val()), 0 > c && (c = 0), c > p && (p = c));
                var u = a("input#nasa_hasSearch").length > 0 && "" !== a("input#nasa_hasSearch").val() ? 1 : 0,
                    m = 1 === u ? a("input#nasa_hasSearch").val() : "";
                nasa_Ajax_filter(a, r, n, s, e, o, T, c, p, u, m, t, i)
            }
            return !1
        }
    }), a("body").on("click", ".nasa-change-layout", function() {
        var t = a(this);
        return a(t).hasClass("active") ? !1 : (changeLayoutShopPage(a, t), void 0)
    });
    var q = a.cookie("gridcookie");
    if ("undefined" != typeof q && a(".nasa-change-layout." + q).click(), a("body").on("click", ".nasa_logout_menu a", function() {
            a('input[name="nasa_logout_menu"]').length > 0 && (window.location.href = a('input[name="nasa_logout_menu"]').val())
        }), a("body").on("click", ".nasa_show_manual > a", function() {
            var t = a(this),
                e = a(t).attr("data-show"),
                n = a(t).parent(),
                s = a(n).attr("data-delay") ? parseInt(a(n).attr("data-delay")) : 100,
                i = "1" === a(n).attr("data-fadein") ? !0 : !1;
            "1" === e ? (a(n).parent().find(".nasa-show-less").each(function() {
                i ? a(this).fadeIn(s) : a(this).slideDown(s)
            }), a(t).fadeOut(350), a(n).find(".nasa-hidden").fadeIn(350)) : (a(n).parent().find(".nasa-show-less").each(function() {
                !a(this).hasClass("nasa-chosen") && !a(this).find(".nasa-active").length > 0 && (i ? a(this).fadeOut(s) : a(this).slideUp(s))
            }), a(t).fadeOut(350), a(n).find(".nasa-show").fadeIn(350))
        }), a("body").on("click", ".nasa-switch-register", function() {
            a("#nasa-login-register-form .nasa-message").html(""), a(".nasa_register-form, .register-form").animate({
                left: "0"
            }, 350), a(".nasa_login-form, .login-form").animate({
                left: "-100%"
            }, 350), setTimeout(function() {
                a(".nasa_register-form, .register-form").css({
                    position: "relative"
                }), a(".nasa_login-form, .login-form").css({
                    position: "absolute"
                })
            }, 350)
        }), a("body").on("click", ".nasa-switch-login", function() {
            a("#nasa-login-register-form .nasa-message").html(""), a(".nasa_register-form, .register-form").animate({
                left: "100%"
            }, 350), a(".nasa_login-form, .login-form").animate({
                left: "0"
            }, 350), setTimeout(function() {
                a(".nasa_register-form, .register-form").css({
                    position: "absolute"
                }), a(".nasa_login-form, .login-form").css({
                    position: "relative"
                })
            }, 350)
        }), a(".nasa-login-register-ajax").length > 0 && a("#nasa-login-register-form").length > 0 && (a("body").on("click", ".nasa-login-register-ajax", function() {
            if ("1" === a(this).attr("data-enable") && a("#customer_login").length <= 0) {
                var t = a("#nasa-menu-sidebar-content").outerWidth().toString();
                return a("#nasa-menu-sidebar-content").animate({
                    left: "-" + t + "px",
                    "z-index": 399
                }, 700), a("#mobile-navigation").attr("data-show", "0"), a(".black-window").fadeIn(200).removeClass("nasa-transparent").addClass("desk-window"), a(".nasa-login-register-warper").show().animate({
                    top: "15px"
                }, 700), !1
            }
        }), a("body").on("click", '.nasa_login-form input[type="submit"][name="nasa_login"]', function() {
            var t = a(this).parents("form.login"),
                e = a(t).serializeArray();
            return a.ajax({
                url: ajaxurl,
                type: "post",
                dataType: "json",
                data: {
                    action: "nasa_process_login",
                    data: e,
                    login: !0
                },
                beforeSend: function() {
                    a("#nasa-login-register-form #nasa_customer_login").css({
                        opacity: .3
                    }), a("#nasa-login-register-form #nasa_customer_login").after('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>')
                },
                success: function(t) {
                    a("#nasa-login-register-form #nasa_customer_login").css({
                        opacity: 1
                    }), a("#nasa-login-register-form").find(".nasa-loader, .please-wait").remove();
                    var e = "0" === t.error ? "nasa-success" : "nasa-error";
                    a("#nasa-login-register-form .nasa-message").html('<h4 class="' + e + '">' + t.mess + "</h4>"), "0" === t.error ? (a("#nasa-login-register-form .nasa-form-content").remove(), window.location.href = t.redirect) : "error" === t._wpnonce && setTimeout(function() {
                        window.location.reload()
                    }, 2e3)
                }
            }), !1
        }), a("body").on("click", '.nasa_register-form input[type="submit"][name="nasa_register"]', function() {
            var t = a(this).parents("form.register"),
                e = a(t).serializeArray();
            return a.ajax({
                url: ajaxurl,
                type: "post",
                dataType: "json",
                data: {
                    action: "nasa_process_register",
                    data: e,
                    register: !0
                },
                beforeSend: function() {
                    a("#nasa-login-register-form #nasa_customer_login").css({
                        opacity: .3
                    }), a("#nasa-login-register-form #nasa_customer_login").after('<div class="nasa-loader"><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div><div class="nasa-line"></div></div>')
                },
                success: function(t) {
                    a("#nasa-login-register-form #nasa_customer_login").css({
                        opacity: 1
                    }), a("#nasa-login-register-form").find(".nasa-loader, .please-wait").remove();
                    var e = "0" === t.error ? "nasa-success" : "nasa-error";
                    a("#nasa-login-register-form .nasa-message").html('<h4 class="' + e + '">' + t.mess + "</h4>"), "0" === t.error ? (a("#nasa-login-register-form .nasa-form-content").remove(), window.location.href = t.redirect) : "error" === t._wpnonce && setTimeout(function() {
                        window.location.reload()
                    }, 2e3)
                }
            }), !1
        })), a("body").on("click", ".btn-combo-link", function() {
            var t = a(window).outerWidth(),
                e = a(this),
                n = a(e).attr("data-show_type"),
                s = a(e).parents(".products.list");
            switch ((946 > t || 1 === a(s).length) && (n = "popup"), n) {
                default: loadComboPopup(a, e)
            }
            return !1
        }), a(".nasa-upsell-product-detail").find(".nasa-upsell-slider").length > 0 && a(".nasa-upsell-product-detail").find(".nasa-upsell-slider").owlCarousel({
            loop: !0,
            nav: !0,
            dots: !1,
            autoplay: !0,
            autoplaySpeed: 600,
            navSpeed: 600,
            autoplayHoverPause: !0,
            navText: ["", ""],
            responsive: {
                0: {
                    items: 1
                }
            }
        }), a(".nasa-active").length > 0 && a(".nasa-active").each(function() {
            1 === a(this).parents(".nasa-show-less").length && a(this).parents(".nasa-show-less").show()
        }), a(".nasa-top-sidebar").length > 0 && (initNasaTopSidebar(a), a("body").on("click", ".nasa-tab-filter-topbar-categories", function() {
            var t = a(this);
            if (a(".filter-cat-icon-mobile").click(), "0" === a(t).attr("data-top_icon")) {
                var e = a(t).attr("data-widget"),
                    n = a(".nasa-top-sidebar"),
                    s = a(e).hasClass("nasa-active") ? !0 : !1;
                a(t).parents(".nasa-top-row-filter").find("> li").removeClass("nasa-active"), a(n).find(".nasa-widget-wrap").removeClass("nasa-active").slideUp(300), s || (a(e).addClass("nasa-active").slideDown(300), a(t).parents("li").addClass("nasa-active"))
            } else a(".site-header").find(".filter-cat-icon").click(), (a(".nasa-header-sticky").length <= 0 || a(".sticky-wrapper").length && !a(".sticky-wrapper").hasClass("fixed-already")) && a("html, body").animate({
                scrollTop: 0
            }, 700);
            initTopCategoriesFilter(a)
        }), a("body").on("click", ".nasa-top-row-filter a.nasa-tab-filter-topbar", function() {
            var t = a(this);
            topFilterClick(a, t, "animate")
        }), a("body").on("click", ".nasa-ignore-variation-item", function() {
            var t = a(this).attr("data-term_id");
            a('.nasa-filter-by-variations.nasa-filter-var-chosen[data-term_id="' + t + '"]').length > 0 && (a(".nasa-has-filter-ajax").length < 1 ? window.location.href = a('.nasa-filter-by-variations.nasa-filter-var-chosen[data-term_id="' + t + '"]').attr("href") : a('.nasa-filter-by-variations.nasa-filter-var-chosen[data-term_id="' + t + '"]').click())
        })), a(".nasa-top-sidebar-2").length > 0 && (initNasaTopSidebar2(a), a("body").on("click", ".nasa-toggle-top-bar-click", function() {
            var t = a(this);
            topFilterClick2(a, t, "animate")
        })), "undefined" == typeof nasa_next_prev && a("body").on("click", ".nasa-nav-icon-slider", function() {
            var t = a(this),
                e = a(t).parents(".nasa-nav-carousel-wrap"),
                n = a(t).attr("data-do"),
                s = a(e).attr("data-id");
            if (1 === a(s).length) switch (n) {
                case "next":
                    a(s).find(".owl-nav .owl-next").click();
                    break;
                case "prev":
                    a(s).find(".owl-nav .owl-prev").click()
            }
        }), initThemeNasaGiftFeatured(a), a("body").on("click", ".nasa-gift-featured-event", function() {
            var t = a(this).parents(".product-item");
            1 === a(t).find(".nasa-product-grid .btn-combo-link").length ? a(t).find(".nasa-product-grid .btn-combo-link").click() : 1 === a(t).find(".nasa-product-list .btn-combo-link").length && a(t).find(".nasa-product-list .btn-combo-link").click()
        }), a(".nasa-content-combo-gift .nasa-combo-slider").length > 0) {
        var I = a(".nasa-content-combo-gift .nasa-combo-slider");
        loadCarouselCombo(a, I, 4)
    }
    if (1 === a(".site-header .static-block-wrapper").find(".support-show").length && (a("body").on("click", ".site-header .static-block-wrapper .support-show", function() {
            a(".site-header .static-block-wrapper").find(".nasa-transparent-topbar").length <= 0 && a(".site-header .static-block-wrapper").append('<div class="nasa-transparent-topbar"></div>'), 1 === a(".site-header .static-block-wrapper").find(".support-hide").length && (a(".site-header .static-block-wrapper .support-hide").hasClass("active") ? (a(".static-block-wrapper .support-hide").removeClass("active").fadeOut(300), a(".site-header .static-block-wrapper .nasa-transparent-topbar").hide()) : (a(".static-block-wrapper .support-hide").addClass("active").fadeIn(300), a(".site-header .static-block-wrapper .nasa-transparent-topbar").show()))
        }), a("body").on("click", ".site-header .static-block-wrapper .nasa-transparent-topbar", function() {
            a(".static-block-wrapper .support-hide").removeClass("active").fadeOut(300), a(".site-header .static-block-wrapper .nasa-transparent-topbar").hide()
        })), a('select[name="nasa_switch_languages"]').length > 0 && a("body").on("change", 'select[name="nasa_switch_languages"]', function() {
            var t = a(this).attr("data-active");
            if ("1" === t) {
                var e = a(this).val();
                window.location.href = e
            }
        }), a("body").on("click", ".nasa-product-details-page .scroll-to-reviews", function() {
            if (1 === a(".nasa-product-details-page .product-details .nasa-tabs-content .reviews_tab > a").length) {
                var t = a(".nasa-product-details-page .product-details .nasa-tabs-content .reviews_tab").offset().top;
                a("html, body").animate({
                    scrollTop: t - 200
                }, 500), setTimeout(function() {
                    a(".nasa-product-details-page .product-details .nasa-tabs-content .reviews_tab > a").click()
                }, 500)
            }
            return !1
        }), a(".nasa-logo-retina").length > 0) {
        var P = window.devicePixelRatio ? window.devicePixelRatio : 1;
        P > 1 && setTimeout(function() {
            var t, e, n = "";
            a(".nasa-logo-retina img").each(function() {
                if (("undefined" == typeof n || "" === n) && (n = a(this).attr("data-src-retina")), "undefined" != typeof n && "" !== n) {
                    var s = 1 === a(this).parents(".nasa-no-fix-size-retina").length ? !1 : !0;
                    if (t = e = "auto", s) {
                        var i = parseInt(a(this).attr("width"));
                        t = i ? i : a(this).width();
                        var r = parseInt(a(this).attr("height"));
                        e = r ? r : a(this).height()
                    }(t && e || "auto" === t) && (a(this).css("width", t), a(this).css("height", e), a(this).attr("src", n), a(this).removeAttr("srcset"))
                }
            })
        }, 1e3)
    }
    if (initTopCategoriesFilter(a), hoverTopCategoriesFilter(a), hoverChilrenTopCatogoriesFilter(a), a("body").on("click", ".filter-cat-icon", function() {
            var t = a(this);
            a(t).hasClass("nasa-disable") || (a(t).addClass("nasa-disable"), a(".nasa-elements-wrap").addClass("nasa-invisible"), a("#header-content .nasa-top-cat-filter-wrap").addClass("nasa-show"), a(".nasa-has-filter-ajax").length <= 0 && a(".header-wrapper .nasa-top-cat-filter-wrap").before('<div class="nasa-tranparent-filter nasa-hide-for-mobile" />'), setTimeout(function() {
                a(t).removeClass("nasa-disable")
            }, 600))
        }), a("body").on("click", ".filter-cat-icon-mobile", function() {
            var t = a(this);
            if (!a(t).hasClass("nasa-disable")) {
                a(t).addClass("nasa-disable"), a(".nasa-top-cat-filter-wrap-mobile").addClass("nasa-show"), a(".transparent-mobile").fadeIn(300), setTimeout(function() {
                    a(t).removeClass("nasa-disable")
                }, 600);
                var e = a(".nasa-check-reponsive.nasa-switch-check").length && 1 === a(".nasa-check-reponsive.nasa-switch-check").width() ? !0 : !1;
                if (e && a(".nasa-top-cat-filter-wrap-mobile").find(".nasa-top-cat-filter").length <= 0 && a("#nasa-main-cat-filter").length && a("#nasa-mobile-cat-filter").length) {
                    var n = a("#nasa-main-cat-filter").clone().html();
                    a("#nasa-mobile-cat-filter").html(n)
                }
            }
        }), a("body").on("click", ".nasa-close-filter-cat, .nasa-tranparent-filter", function() {
            a(".nasa-elements-wrap").removeClass("nasa-invisible"), a("#header-content .nasa-top-cat-filter-wrap").removeClass("nasa-show"), a(".nasa-tranparent-filter").remove()
        }), a("body").on("click", ".nasa-show-coupon", function() {
            1 === a(".nasa-coupon-wrap").length && (a(".nasa-coupon-wrap").toggleClass("nasa-active"), setTimeout(function() {
                a('.nasa-coupon-wrap.nasa-active input[name="coupon_code"]').focus()
            }, 100))
        }), cloneGroupBtnsProductItem(a), loadScrollSingleProduct(a), loadProductsMasonryIsotope(a), loadPostsMasonryIsotope(a), initWishlistIcons(a), initCompareIcons(a), a(".nasa-topbar-wrap.nasa-topbar-toggle").length && a("body").on("click", ".nasa-topbar-wrap .nasa-icon-toggle", function() {
            var t = a(this).parents(".nasa-topbar-wrap");
            a(t).toggleClass("nasa-topbar-hide")
        }), a("body").on("click", ".black-window-mobile", function() {
            a(this).removeClass("nasa-push-cat-show"), a(".nasa-push-cat-filter").removeClass("nasa-push-cat-show"), a(".nasa-products-page-wrap").removeClass("nasa-push-cat-show")
        }), a("body").on("click", ".nasa-widget-show-more a.nasa-widget-toggle-show", function() {
            var t = a(this).attr("data-show"),
                e = "";
            "0" === t ? (e = a('input[name="nasa-widget-show-less-text"]').length ? a('input[name="nasa-widget-show-less-text"]').val() : "Less -", a(this).attr("data-show", "1"), a(".nasa-widget-toggle.nasa-widget-show-less").addClass("nasa-widget-show")) : (e = a('input[name="nasa-widget-show-more-text"]').length ? a('input[name="nasa-widget-show-more-text"]').val() : "More +", a(this).attr("data-show", "0"), a(".nasa-widget-toggle.nasa-widget-show-less").removeClass("nasa-widget-show")), a(this).html(e)
        }), a("body").on("click", ".nasa-mobile-icons-wrap .nasa-toggle-mobile_icons", function() {
            a(this).parents(".nasa-mobile-icons-wrap").toggleClass("nasa-hide-icons")
        }), 1 === a(".nasa-product-details-page .nasa-gallery-variation-supported").length ? changeGalleryVariableSingleProduct(a) : (loadSlickSingleProduct(a), loadGalleryPopup(a), changeImageVariableSingleProduct(a)), a("body").on("click", ".product-lightbox-btn", function(t) {
            a(".nasa-single-product-slide").length > 0 ? a(".product-images-slider").find(".slick-current.slick-active a").click() : a(".nasa-single-product-scroll").length > 0 && a("#nasa-main-image-0 a").click(), t.preventDefault()
        }), a("body").on("click", "a.product-video-popup", function(t) {
            a(".product-images-slider").find(".first a").click();
            var e = a.magnificPopup.instance;
            e.prev(), t.preventDefault()
        }), a('input[name="nasa_fixed_single_add_to_cart"]').length && "1" === a('input[name="nasa_fixed_single_add_to_cart"]').val() & a(".nasa-product-details-page").length && a(".nasa-add-to-cart-fixed").length < 1) {
        a("body").append('<div class="nasa-add-to-cart-fixed"><div class="nasa-wrap-content-inner"><div class="nasa-wrap-content"></div></div></div>');
        var F = a(".nasa-add-to-cart-fixed .nasa-wrap-content");
        if (a(F).append('<div class="nasa-fixed-product-info"></div>'), a(".nasa-product-details-page .product-thumbnails").length) {
            var A = a(".nasa-product-details-page .product-thumbnails .nasa-wrap-item-thumb:eq(0)").attr("data-thumb_org") || a(".nasa-product-details-page .product-thumbnails .nasa-wrap-item-thumb:eq(0) img").attr("src");
            a(".nasa-fixed-product-info").append('<div class="nasa-thumb-clone"><img src="' + A + '" /></div>')
        }
        if (a(".nasa-product-details-page .product-info .product_title").length) {
            var O = a(".nasa-product-details-page .product-info .product_title").html();
            a(".nasa-fixed-product-info").append('<div class="nasa-title-clone"><h3>' + O + "</h3></div>")
        }
        if (a(".nasa-product-details-page .product-info .price").length) {
            var z = a(".nasa-product-details-page .product-info .price").html();
            a(".nasa-title-clone").length ? a(".nasa-title-clone").append('<span class="price">' + z + "</span>") : a(".nasa-fixed-product-info").append('<div class="nasa-title-clone"><span class="price">' + z + "</span></div>")
        }
        if (a(".nasa-product-details-page .variations_form").length) {
            a(F).append('<div class="nasa-fixed-product-variations-wrap"><div class="nasa-fixed-product-variations"></div></div>');
            var D = 1,
                W = 1;
            a(".nasa-product-details-page .variations_form .variations tr").each(function() {
                var t = a(this),
                    e = "nasa-attr-wrap-" + D.toString(),
                    n = a(t).find("select").attr("data-attribute_name") || a(t).find("select").attr("name");
                if (a(t).find(".nasa-attr-ux_wrap").length) a(".nasa-fixed-product-variations").append('<div class="nasa-attr-ux_wrap-clone ' + e + '"></div>'), a(t).find(".nasa-attr-ux").each(function() {
                    var t = a(this),
                        s = "nasa-attr-ux-" + W.toString(),
                        i = "nasa-attr-ux-clone-" + W.toString(),
                        r = i;
                    a(t).hasClass("nasa-attr-ux-image") && (i += " nasa-attr-ux-image-clone"), a(t).hasClass("nasa-attr-ux-color") && (i += " nasa-attr-ux-color-clone"), a(t).hasClass("nasa-attr-ux-label") && (i += " nasa-attr-ux-label-clone");
                    var o = a(t).hasClass("selected") ? " selected" : "",
                        l = a(t).html();
                    a(t).addClass(s), a(t).attr("data-target", "." + r), a(".nasa-attr-ux_wrap-clone." + e).append('<a href="javascript:void(0);" class="nasa-attr-ux-clone' + o + " " + i + " nasa-" + n + '" data-target=".' + s + '">' + l + "</a>"), W++
                });
                else {
                    a(".nasa-fixed-product-variations").append('<div class="nasa-attr-select_wrap-clone ' + e + '"></div>');
                    var s = a(t).find("select"),
                        i = a(t).find(".label").length ? a(t).find(".label").html() : "",
                        r = "nasa-attr-select-" + W.toString(),
                        o = "nasa-attr-select-clone-" + W.toString(),
                        l = a(s).html();
                    a(s).addClass(r).addClass("nasa-attr-select"), a(s).attr("data-target", "." + o), a(".nasa-attr-select_wrap-clone." + e).append(i + '<select name="' + n + '" class="nasa-attr-select-clone ' + o + " nasa-" + n + '" data-target=".' + r + '">' + l + "</select>"), W++
                }
                D++
            })
        } else a(F).addClass("nasa-fixed-single-simple");
        setTimeout(function() {
            var t = nasa_clone_add_to_cart(a);
            a(F).append('<div class="nasa-fixed-product-btn"></div>'), a(".nasa-fixed-product-btn").html(t);
            var e = a('.nasa-product-details-page form.cart input[name="quantity"]').val();
            a('.nasa-single-btn-clone input[name="quantity"]').val(e)
        }, 250), setTimeout(function() {
            a(".nasa-attr-ux").length && a(".nasa-attr-ux").each(function() {
                var t = a(this),
                    e = a(t).attr("data-target");
                if (a(e).length) {
                    var n = a(t).hasClass("nasa-disable") ? !0 : !1;
                    n ? a(e).hasClass("nasa-disable") || a(e).addClass("nasa-disable") : a(e).removeClass("nasa-disable")
                }
            })
        }, 550), a("body").on("click", ".nasa-attr-ux", function() {
            var t = a(this).attr("data-target");
            if (a(t).length) {
                var e = a(t).parents(".nasa-attr-ux_wrap-clone");
                a(e).find(".nasa-attr-ux-clone").removeClass("selected"), a(this).hasClass("selected") && a(t).addClass("selected"), a(".nasa-fixed-product-btn").length && setTimeout(function() {
                    var t = nasa_clone_add_to_cart(a);
                    a(".nasa-fixed-product-btn").html(t);
                    var e = a('.nasa-product-details-page form.cart input[name="quantity"]').val();
                    a('.nasa-single-btn-clone input[name="quantity"]').val(e)
                }, 250), setTimeout(function() {
                    a(".nasa-attr-ux").length && a(".nasa-attr-ux").each(function() {
                        var t = a(this),
                            e = a(t).attr("data-target");
                        if (a(e).length) {
                            var n = a(t).hasClass("nasa-disable") ? !0 : !1;
                            n ? a(e).hasClass("nasa-disable") || a(e).addClass("nasa-disable") : a(e).removeClass("nasa-disable")
                        }
                    })
                }, 250)
            }
        }), a("body").on("click", ".nasa-attr-ux-clone", function() {
            var t = a(this).attr("data-target");
            a(t).length && a(t).click()
        }), a("body").on("change", ".nasa-attr-select", function() {
            var t = a(this),
                e = a(t).attr("data-target"),
                n = a(t).val();
            a(e).length && (setTimeout(function() {
                var s = a(t).html();
                a(e).html(s), a(e).val(n)
            }, 100), setTimeout(function() {
                var t = nasa_clone_add_to_cart(a);
                a(".nasa-fixed-product-btn").html(t);
                var e = a('.nasa-product-details-page form.cart input[name="quantity"]').val();
                a('.nasa-single-btn-clone input[name="quantity"]').val(e), a(".nasa-attr-ux").length && a(".nasa-attr-ux").each(function() {
                    var t = a(this),
                        e = a(t).attr("data-target");
                    if (a(e).length) {
                        var n = a(t).hasClass("nasa-disable") ? !0 : !1;
                        n ? a(e).hasClass("nasa-disable") || a(e).addClass("nasa-disable") : a(e).removeClass("nasa-disable")
                    }
                })
            }, 250))
        }), a("body").on("change", ".nasa-attr-select-clone", function() {
            var t = a(this).attr("data-target"),
                e = a(this).val();
            a(t).length && a(t).val(e).change()
        }), a("body").on("click", ".reset_variations", function() {
            a(F).find(".selected").removeClass("selected"), setTimeout(function() {
                var t = nasa_clone_add_to_cart(a);
                a(".nasa-fixed-product-btn").html(t);
                var e = a('.nasa-product-details-page form.cart input[name="quantity"]').val();
                a('.nasa-single-btn-clone input[name="quantity"]').val(e), a(".nasa-attr-ux").length && a(".nasa-attr-ux").each(function() {
                    var t = a(this),
                        e = a(t).attr("data-target");
                    if (a(e).length) {
                        var n = a(t).hasClass("nasa-disable") ? !0 : !1;
                        n ? a(e).hasClass("nasa-disable") || a(e).addClass("nasa-disable") : a(e).removeClass("nasa-disable")
                    }
                })
            }, 250)
        }), a("body").on("click", ".nasa-product-details-page form.cart .quantity .plus, .nasa-product-details-page form.cart .quantity .minus", function() {
            if (a('.nasa-single-btn-clone input[name="quantity"]').length) {
                var t = a('.nasa-product-details-page form.cart input[name="quantity"]').val();
                a('.nasa-single-btn-clone input[name="quantity"]').val(t)
            }
        }), a("body").on("click", ".nasa-single-btn-clone .plus", function() {
            a(".nasa-product-details-page form.cart .quantity .plus").length && a(".nasa-product-details-page form.cart .quantity .plus").click()
        }), a("body").on("click", ".nasa-single-btn-clone .minus", function() {
            a(".nasa-product-details-page form.cart .quantity .minus").length && a(".nasa-product-details-page form.cart .quantity .minus").click()
        }), a("body").on("keyup", '.nasa-product-details-page form.cart input[name="quantity"]', function() {
            var t = a(this).val();
            a('.nasa-single-btn-clone input[name="quantity"]').val(t)
        }), a("body").on("keyup", '.nasa-single-btn-clone input[name="quantity"]', function() {
            var t = a(this).val();
            a('.nasa-product-details-page form.cart input[name="quantity"]').val(t)
        }), a("body").on("click", ".nasa-single-btn-clone .single_add_to_cart_button", function() {
            a(".nasa-product-details-page form.cart .single_add_to_cart_button").length && a(".nasa-product-details-page form.cart .single_add_to_cart_button").click()
        })
    }
    a("body").on("click", ".nasa-buy-now", function() {
        if (!a(this).hasClass("nasa-waiting")) {
            a(this).addClass("nasa-waiting");
            var t = a(this).parents("form.cart");
            a(t).find(".single_add_to_cart_button.disabled").length ? (a(t).find(".single_add_to_cart_button.disabled").click(), a(this).removeClass("nasa-waiting")) : a(t).find('input[name="nasa_buy_now"]').length && (a('input[name="nasa-enable-addtocart-ajax"]').length && a('input[name="nasa-enable-addtocart-ajax"]').val("0"), a(t).find('input[name="nasa_buy_now"]').val("1"), a(t).find(".single_add_to_cart_button").click())
        }
        return !1
    })
});