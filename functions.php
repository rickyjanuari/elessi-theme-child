<?php
/**
 * theme- functions and definitions
 */

 
function theme_child_enqueue_styles() {

    $parent_style = 'elessi-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'elessi-child-style',
        get_stylesheet_directory_uri() . '/style.min.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'theme_child_enqueue_styles' );

/*
 * Enqueue JavaScript
 */

function theme_child_scripts() {
    wp_register_script( 'custom', get_stylesheet_directory_uri() . '/js/dist/custom.min.js', array(), null, true);
    wp_register_script( 'scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', array(), null, true);
    wp_enqueue_script('custom');
    wp_enqueue_script('scripts');
}
add_action( 'wp_enqueue_scripts', 'theme_child_scripts' );

function theme_child_customizer(){
  wp_register_script( 'customizer', get_stylesheet_directory_uri() . '/js/customizer.js', array(), null, true);

  wp_enqueue_script('customizer');
}
add_action( 'customize_preview_init', 'theme_child_customizer' );




/**
 * Setup Child Theme's textdomain.
 *
 * Declare textdomain for this child theme.
 * Translations can be filed in the /languages/ directory.
 */
function theme_child_theme_setup() {
    load_child_theme_textdomain( 'theme-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'theme_child_theme_setup' );

if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
    wp_register_script('livereload', 'http://mecca570.test:35729/livereload.js?snipver=1', null, false, true);
    wp_enqueue_script('livereload');
  }