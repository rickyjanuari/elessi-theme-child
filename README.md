
**Quick start**  
1\. Fork & clone the git repo, or download zip file directly.  

2\. Go to style.css file and change *Template: to_your_parent_theme_name*.

3\. Before you start to use you must have installed npm, gulp and bower. If you don't have installed yet, install [Npm here](https://www.npmjs.com/get-npm) and [Gulp here](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md).

4\. Run the `npm install` command on your console.

6\. Run the `gulp install` command on your console.  

7\. Start his new project - just write `gulp` and after `gulp dev` on your console and watch for changes.  
